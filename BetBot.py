import requests
import json
import sys
import pprint

accountsApi = "https://api.betfair.com/exchange/account/rest/v1.0/"
betsApi = "https://api.betfair.com/exchange/betting/rest/v1.0/"

EPL = 31


class Session:
    def __init__(self):
        self.token = ""
        self.appKey = ""


def new_session(username, password, appKey):
    # (str, str, str) -> Session
    session = Session()
    session.token = login(username, password)
    session.appKey = appKey
    return session


def run():
    session = new_session(sys.argv[1], sys.argv[2], sys.argv[3])
    events = list_events(session, EPL)

    # for each event
    # marketCat = listMarketCatalogue(session, event)

    # for each market


def get_home_odd(session, event_id):
    # (Session, str) -> float
    market_catalog = list_market_catalogue(session, event_id)
    marketId = market_catalog[0]['marketId']
    market_book = list_market_book(session, marketId)
    pprint.pprint(market_book)
    return market_book[0]['runners'][0]['lastPriceTraded']


def list_events(session, competition_id):
    # (Session, int) -> {}
    data = { "filter": {
               "marketTypeCodes": ["MATCH_ODDS"],
               "competitionIds": [competition_id]
            }}
    return get_data(session, "listEvents/", data=data)


def list_market_catalogue(session, event_id):
    # (Session, str) -> {}
    data = {
        "filter": {
            "marketTypeCodes": ["MATCH_ODDS"],
            "eventIds" : [event_id]
        },
        "maxResults": "200",
        "marketProjection": ["COMPETITION"]
    }
    return get_data(session, "listMarketCatalogue/", data)


def list_market_book(session, marketId):
    # (Session, str) -> {}
    data = {
        "marketIds": [marketId],
        "priceProjection":{"priceData":["SP_TRADED"] }
    }
    return get_data(session, "listMarketBook/", data)


def main(username, password):
    # (str, str) -> None
    session = new_session(username, password, "UMiYZQkEl3mOevMe")
    print get_data(session, "listEventTypes/", {'filter': {}})


def get_developer_keys(session):
    # (Session) -> {}
    endpoint = accountsApi + "getDeveloperAppKeys/"
    header = {'X-Authentication': session.token, 'content-type':'application/json'}
    resp = requests.post(endpoint, headers=header)
    return resp.json()


def create_developer_keys(session):
    # (Session) -> {}
    endpoint = accountsApi + "createDeveloperAppKeys/"
    header = {'X-Authentication': session.token, 'content-type':'application/json'}
    resp = requests.post(endpoint, headers=header, data='betBot')
    return resp.json()


def get_data(session, command, data):
    # (str, str, str, {}) -> {}
    endpoint = betsApi + command
    header = {'X-Application': session.appKey, 'X-Authentication': session.token, 'content-type':'application/json'}
    resp = requests.post(endpoint, data=json.dumps(data), headers=header)
    return resp.json()


def login(username, password):
    # (str, str)-> str
    payload = 'username=%s&password=%s' % (username, password)
    certs = ('keys/client-2048.crt', 'keys/client-2048.key')
    headers = {'X-Application': 'betbot_rizvnn', 'Content-Type': 'application/x-www-form-urlencoded'}
    resp = requests.post('https://identitysso.betfair.com/api/certlogin', data=payload, cert= certs, headers=headers)

    if resp.status_code == 200:
        return resp.json()['sessionToken']
    else:
        raise Exception("Request Failed")

if __name__== "__main__":
    main(sys.argv[1], sys.argv[2])